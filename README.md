# Glints Indonesia
## _oleh: Mirza Purnandi_

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)
<p><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

Membuat CRUD REST API untuk proses Parkir. 

### Required:
- PHP 7.4
- MySQL

## Installer
-   `git clone https://gitlab.com/mirzapurnandi/parkiran.git nama_folder`
-   `cd nama_folder`
-   `composer install`
-   `cp .env-example .env`
-   `php artisan key:generate`
-   `php artisan jwt:secret`
-   `php artisan migrate`
-   `php artisan db:seed`

Ikuti Langkah Berikut:
- Ubah file .env didalam folder "nama_folder"
    ```sh
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=nama_database_anda
    DB_USERNAME=username_database_anda
    DB_PASSWORD=password_database_anda
    ```
- Selesai, Jalankan `php artisan serve` di command

## CARA PENGGUNAAN
Gunakan Postman untuk dapat menjalankan API diatas.
di folder juga sudah di sediakan file .json untuk dapat di import ke dalam postman 
([GlintsMirza.postman_collection.json](https://gitlab.com/mirzapurnandi/parkiran/-/blob/main/GlintsMirza.postman_collection.json))

### Login
| POST | Login | http://localhost:8000/api/login |
| ------ | ------ | ------ | 
```sh
body {
    "email": "admin@gmail.com",
    "password": "password" 
}
```
gunakan data diatas untuk user Admin

```sh
body {
    "email": "pengguna@gmail.com",
    "password": "password" 
}
```
gunakan data diatas untuk user Pengguna

### Transaction
#### Transaction Index
| GET | Transaction Index | http://localhost:8000/api/transaction |
| ------ | ------ | ------ | 
```sh
header {
    "Authorization": "Bearer <token>",
    "Accept": "application/json"
}

body {
    "date_start": "2023-07-08",
    "date_end": "2023-07-08"
}
```
Gunakan Selalu header Accept untuk mendapatkan response berupa Json, lalu gunakan Authorization sebagai kunci masuk. Tempelkan token yang didapatkan setelah login tadi lalu letakan di sebelah Bearer. Gunakan body 'date_start' dan 'date_end' untuk dapat memfilter berdasarkan tanggal.

#### Transaction Store
| POST | Transaction Index | http://localhost:8000/api/transaction |
| ------ | ------ | ------ | 
```sh
header {
    "Authorization": "Bearer <token>",
    "Accept": "application/json"
}

body {
    "nopol": "B1909QWE"
}
```
Gunakan Selalu header Accept untuk mendapatkan response berupa Json, lalu gunakan Authorization sebagai kunci masuk. Tempelkan token yang didapatkan setelah login tadi lalu letakan di sebelah Bearer. Gunakan body 'nopol' untuk melakukan insert data kendaraan untuk menambahkan data kendaraan yang masuk ke parkiran.

#### Transaction Update
| PUT | Transaction Index | http://localhost:8000/api/transaction |
| ------ | ------ | ------ | 
```sh
header {
    "Authorization": "Bearer <token>",
    "Accept": "application/json"
}

body {
    "code": "RRZUF8"
}
```
Gunakan Selalu header Accept untuk mendapatkan response berupa Json, lalu gunakan Authorization sebagai kunci masuk. Tempelkan token yang didapatkan setelah login tadi lalu letakan di sebelah Bearer. Gunakan body 'code' untuk melakukan Update data kendaraan untuk kendaraan yang akan keluar parkiran.
