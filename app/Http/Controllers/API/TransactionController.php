<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Carbon\Carbon;

class TransactionController extends Controller
{
    public function index(Request $request)
    {
        try {
            $transaction = Transaction::orderBy('date_start');
            if ($request->date_start) {
                $validator = Validator::make($request->all(), [
                    'date_start' => 'nullable',
                    'date_end' => 'nullable|required_with:date_start|after_or_equal:date_start',
                ], [
                    'date_end.date_start' => 'Tanggal Akhir harus diisi',
                    'date_end.after_or_equal' => 'Tanggal Akhir harus lebih besar dari Tanggal Awal'
                ]);

                if ($validator->fails()) {
                    return $this->errorResponse($validator->errors(), 'Error Validation', 401);
                }

                $date_start = date('Y-m-d H:i:s', strtotime($request->date_start));
                $date_end = date('Y-m-d H:i:s', strtotime($request->date_end . '23:23:59'));
                $transaction = $transaction->whereBetween('date_start', [$date_start, $date_end]);
            }
            $transaction = $transaction->simplePaginate(30);
            return $this->successResponse($transaction, 'Successfully Get transaction', 200);
        } catch (\Throwable $th) {
            return $this->errorResponse([], $th->getMessage(), $th->getCode());
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nopol' => 'required',
        ], [
            'nopol.required' => 'Nomor Plat Polisi Harus diisi'
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), 'Error Validation', 401);
        }

        $check = Transaction::where(['nopol' => $request->nopol, 'date_end' => null]);
        if ($check->count() > 0) {
            $message = 'No Plat Polisi ' . $request->nopol . ' Sudah didalam parkiran...';
            return $this->errorResponse($check->first(), $message, 200);
        }

        $code = strtoupper(substr(Str::random(10), 0, 6));
        $transaction = new Transaction();
        $transaction->nopol = $request->nopol;
        $transaction->code = $code;
        $transaction->date_start = now();
        $transaction->save();

        return $this->successResponse($transaction, 'Successfully Save transaction', 201);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|exists:transactions,code',
        ], [
            'code.required' => 'Kode tidak boleh Kosong',
            'code.exists' => 'Kode Unik tidak di temukan di database'
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), 'Error Validation', 401);
        }

        $check_ = Transaction::where(['code' => $request->code]);
        $check = $check_->first();
        if ($check) {
            if ($check->date_end == null) {
                $dateStart = Carbon::parse($check->date_start);
                $dateEnd = Carbon::parse($check->date_end);

                $diff = $dateStart->diff($dateEnd);
                $total_waktu = $diff->h . ' Jam ' . $diff->i . ' Menit';
                $price = ($diff->h + 1) * 3000;
                $datas = [
                    'total_waktu' => $total_waktu,
                    'price' => $price,
                    'date_start' => $check->date_start,
                    'date_end' => date('Y-m-d H:i:s')
                ];
                $check_->update($datas);

                $datas = Transaction::find($check->id);
                return $this->successResponse($datas, 'Anda berhasil Keluar Parkir', 200);
            }
        }

        return $this->errorResponse([], 'Tidak ada Data', 401);
    }
}
