<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'nopol',
        'code',
        'date_start',
        'date_end'
    ];

    protected $dateFormat = 'Y-m-d H:i:s';
}
